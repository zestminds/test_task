<?php
require_once(__DIR__."/config.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
  

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Libraries CSS Files -->
 
  
  <!-- Main Stylesheet File -->
<link href="css/style.css" rel="stylesheet">
</head>
<body id="body" >

  <header>
		<nav class="cls">
			<a href="products.php" >Products</a>
			<a href="population.php" >Population</a>
			<a href="logout.php" >Logout</a>
		</nav>
	</header>
	<div class="row"></div>
	<section class="pop-sec">
  
		<div class="container">
			
			<div class="row">
				<form enctype="multipart/form-data" action="population_process.php" method="post" >
					<div class="col-md-8 mx-auto">
						<h3 class="downld-heading">Population Check - 5 Miles Radius</h3>
						<?php if(isset($_SESSION['population_file_missing'])) { ?>
							<div class="error">Please select a file.</div>
						<?php unset($_SESSION['population_file_missing']); } ?>
						<h3 class="downld-result">Download-Results</h3>
						<?php if (isset($_SESSION['population_failure_file_upload'])) { ?>
							<div class="error">File type is not allowed, Only CSV file is allowed.</div>
						<?php unset($_SESSION['population_failure_file_upload']); } ?>
						<div class="row">
							<div class="col-md-3">
								<label> Csv File to Upload <label>

							</div>
							<div class="col-md-6">
								<div class="file-upload first mb-3">
									<div class="file-select">
										<div class="file-select-button" id="fileName">Choose File</div>
										<div class="file-select-name" id="noFile1">No file chosen...</div> 
										<input type="file" name="upload" id="chooseFile">
									</div>

								</div>

								<button class="btn btn-success" id="up" type="submit" >Download</button>
							</div>
							<div class="col-md-3">

							</div>
						</div>
						<h3 class="downld-result">Upload-Data Results</h3>
						<?php if (isset($_SESSION['population_success'])) { ?>
							<div class="success">Data updated successfully.</div>
						<?php unset($_SESSION['population_success']); } ?>
						<?php if (isset($_SESSION['population_failure'])) { ?>
							<div class="error">Data can not be updated, Please try again.</div>
						<?php unset($_SESSION['population_failure']); } ?>
						<?php if (isset($_SESSION['population_failure_file'])) { ?>
							<div class="error">File type is not allowed, Only CSV file is allowed.</div>
						<?php unset($_SESSION['population_failure_file']); } ?>
						<div class="row">
							<div class="col-md-3">
								<label> Csv File to Upload <label>

							</div>
							<div class="col-md-6">
								<div class="file-upload second mb-3">
									<div class="file-select">
										<div class="file-select-button" id="fileName">Choose File</div>
										<div class="file-select-name" id="noFile2">No file chosen...</div> 
										<input type="file" name="download" id="chooseFile1">

									</div>
								</div>
								<button class="btn btn-danger" type="submit">Upload</button>
							</div>

						</div>
					  
					  
					</div>
				</form>
			</div>
		</div>
	</section>


	
	
	
	
	
	
	
	



   <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>


<script>
$('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".first").removeClass('active');
    $("#noFile1").text("No file chosen..."); 
  }
  else {
    $(".first").addClass('active');
    $(".second").removeClass('active');
    $("#noFile1").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
$('#chooseFile1').bind('change', function () {
  $("#chooseFile").val('');
  $("#noFile1").text("No file chosen..."); 
  var filename = $("#chooseFile1").val();
  if (/^\s*$/.test(filename)) {
	  alert("here");
    $(".second").removeClass('active');
    $("#noFile2").text("No file chosen..."); 
  }
  else {
    $(".first").removeClass('active');
    $(".second").addClass('active');
    $("#noFile2").text(filename.replace("C:\\fakepath\\", "")); 
  }
});

$("#up").on("click",function(){
	setTimeout(function() { $("#chooseFile").val('');$("#noFile1").text("No file chosen..."); $(".first").removeClass('active'); },5000);
});

$( document ).ready(function() {
    window.setTimeout(function(){ $(".error").hide(); $(".success").hide(); },5000);
});

</script>

</body>
</html>
