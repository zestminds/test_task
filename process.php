<?php

require_once(__DIR__."/config.php");
require_once(__DIR__."/functions.php");
$func = new functions();
$func->checklogin($_SESSION);

if ( isset($_POST) && !empty($_FILES['download']['name']) ) {
	if ( !$func->checkFile($_FILES['download']) ) {
		$_SESSION['product_failure_file'] = 1;
		header("Location:products.php");
	} else {
		
		if ( $func->updatecsv($_FILES['download'])) {
			$_SESSION['product_success'] = 1;
			header("Location:products.php");
		} else {
			$_SESSION['product_failure'] = 1;
			header("Location:products.php");
		}
	}
}elseif ( isset($_POST) && !empty($_FILES['upload']['name']) ) {
	if ( !$func->checkFile($_FILES['upload']) ) {
		$_SESSION['product_failure_file_upload'] = 1;
		header("Location:products.php");
	} else {
		$func->readcsv($_FILES['upload']);
	}
} else {
	$_SESSION['product_file_missing'] = 1;
	header("Location:products.php");
}

?>