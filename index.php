<?php require_once(__DIR__."/config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"  rel="stylesheet">
  

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Libraries CSS Files -->
 
  
  <!-- Main Stylesheet File -->
<link href="css/style.css" rel="stylesheet">
</head>
<body id="body" >

  
	<section class="pop-sec">
	  
		<div class="container">
			<h3 class="downld-heading">Login</h3>
			<?php if (isset($_SESSION['login_err'])) {  ?>
				<div class="error">Invalid login credentials, Please try again.</div>
			<?php unset($_SESSION['login_err']); } ?>
			<form action="login.php" method="post">
			  

			  <div class="container">
				<label for="uname"><b>Username</b></label>
				<input type="text" placeholder="Enter Username" name="uname" required>

				<label for="psw"><b>Password</b></label>
				<input type="password" placeholder="Enter Password" name="psw" required>

				<button type="submit">Login</button>
				
			  </div>

			  
			</form>
		</div>



	</section>


	
	
	
	
	
	
	
	



   <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
 
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>


<script>
$('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
$( document ).ready(function() {
    window.setTimeout(function(){ $(".error").hide(); $(".success").hide(); },5000);
});
</script>

</body>
</html>
