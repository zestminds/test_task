<?php
require_once(__DIR__."/config.php");
require_once(__DIR__."/functions.php");
if ( isset($_POST) && !empty($_POST) ) {
	$func = new functions();
	if ( $func->login($_POST) ) {
		$_SESSION['user_id'] =  $func->user;
		header('Location: products.php');
		
	} else { 
		$_SESSION['login_err'] = 1;
		header('Location: index.php');
	}
}
?>