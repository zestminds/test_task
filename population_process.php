<?php

require_once(__DIR__."/config.php");
require_once(__DIR__."/functions.php");
$func = new functions();
$func->checklogin($_SESSION);
if ( isset($_POST) && !empty($_FILES['download']['name']) ) {
	
	if ( !$func->checkFile($_FILES['download']) ) {
		$_SESSION['population_failure_file'] = 1;
		header("Location:population.php");
	} else {
		
		if ( $func->updatecsv($_FILES['download'],false)) {
			$_SESSION['population_success'] = 1;
			header("Location:population.php");
		} else {
			$_SESSION['population_failure'] = 1;
			header("Location:population.php");
		}
	}
}elseif ( isset($_POST) && !empty($_FILES['upload']['name']) ) {
	if ( !$func->checkFile($_FILES['upload']) ) {
		$_SESSION['population_failure_file_upload'] = 1;
		header("Location:population.php");
	} else {
		$func->readcsv($_FILES['upload'],false);
	}
} else {
	$_SESSION['population_file_missing'] = 1;
	header("Location:population.php");
}

?>