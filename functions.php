<?php
require_once(__DIR__."/database.php");

class functions extends database{
	
	public $user = "";
	
	public function pr($arr = array()) {
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	
	public function checkFile($fileArr = array()) {
		if ( !empty($fileArr) ) {
			$allowedExt = array("csv");
			$ext = explode(".",$fileArr['name']);
			$ext = end($ext);
			$ext = strtolower($ext);
			if ( !in_array($ext,$allowedExt) ) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
		
	}
	
	// function to read uploaded CSV file	
	public function readcsv($fileArr = array(), $flag = true) {
		if ( !empty($fileArr) ) {
			// read file from php input directly
			$fileName = $fileArr["tmp_name"];
			if ($fileArr["size"] > 0) {
				$file = fopen($fileName, "r");
				$tmpArr = array();
				while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
					if ( strtolower($column[0]) != 'zip' ) {
						$tmpArr[$column[0]] = $column[0];
					}
				}
				if ( !empty($tmpArr) ) {
					$this->returncsv($tmpArr, $flag);
					return true;
				} else {
					return false;
				}
				
			} else {
				return false;
			}
		}
	}
	
	// function to check existing and non existing data from table and return csv containg records
	public function returncsv($zip = array(), $flag) {
		if ( !empty($zip) ) {
			$tmp = implode(',',$zip);
			if ($flag) {
				$sql = "select zip,product,recorded,org_user,modified_user from products where zip in (".$tmp.")";
			} else {
				$sql = "select zip,five_mile_population,recorded,org_user,modified_user from population where zip in (".$tmp.")";
			}
			$this->connect();
			$result = $this->mysqli->query($sql);
			if ($result -> num_rows > 0) {
				$result -> num_rows;
				$tmpArr  = array();
				while($rows = mysqli_fetch_assoc($result)) {
					$tmpArr[$rows['zip']] = $rows;
				}
				// check if no record is missing from csv in db
				// creating temporaray array to pass in csv
				if ( $result -> num_rows != count($zip) ) {
					$zipArray = array();
					foreach ( $zip as $key=>$val ) {
						if ( isset($tmpArr[$val]) ) {
							$zipArray[] = $tmpArr[$val];
						} else {
							if ( $flag ) { 
								$zipArray[] = array("zip"=>$val,"product"=>"N/A","recorded"=>"N/A","org_user"=>"N/A","modified_user"=>"N/A");
							} else {
								$zipArray[] = array("zip"=>$val,"five_mile_population"=>"N/A","recorded"=>"N/A","org_user"=>"N/A","modified_user"=>"N/A");
							}
						}
					}
					$this->createcsv($zipArray, $flag);
				} else {
					$this->createcsv($tmpArr, $flag);
				}
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	}
	// creating and downloading csv file
	private function createcsv($data = array(), $flag) {
		if (!empty($data)) {
			// output headers so that the file is downloaded rather than displayed
			header('Content-Type: text/csv; charset=utf-8');
			if ($flag) {
				header('Content-Disposition: attachment; filename=product_data.csv');
			} else {
				header('Content-Disposition: attachment; filename=population_data.csv');
			}
			// create a file pointer connected to the output stream
			$output = fopen('php://output', 'w');
			if ( $flag ) {
				$outArray = array('Zip', 'Product', 'Recorded','ORG User','Modified User');
			} else {
				$outArray = array('Zip', '5 Mile Population', 'Recorded','ORG User','Modified User');
			}
			// output the column headings
			fputcsv($output, $outArray);
			foreach ( $data as $key => $val ) {
				fputcsv($output, $val);
			}
		}
		
	}
	
	// function to check if user login is valid
	public function login($data = array()) {
		$pass = $this->encryptpass($data['psw'],$data['uname']);
		$sql = "select id from users where username = '".$data['uname']."' and password = '".$pass."' and is_active =1";
		$this->connect();
		if ($res = $this->mysqli->query($sql)) {
			$row = mysqli_fetch_assoc($res);
			if (isset($row['id']) && !empty($row['id'])) {
				$this->user = $row["id"];
				return true;
			} else {
				return false;
			}				
		} else {
			return false;
		}
		
	}
	
	private function encryptpass($password, $salt = '', $method = 'md5', $crop = true, $start = 4, $end = 10) {
		$salt = empty($salt)?strtotime(date("Y-m-d h:i:s")):$salt;
		if ($crop) {
			$password = $method(substr($method($password.$salt), $start, $end));
		} else {
			$password = $method($password.$salt);
		}
		return $password;
    }
	
	function checklogin($data) {
		if ( !isset($_SESSION['user_id']) ) {
			header('Location: index.php');
		}
	}
	
	// function to parse uploaded csv to insert data
	function updatecsv($fileArr = array(), $flag = true) {
		if ( !empty($fileArr) ) {
			$fileName = $fileArr["tmp_name"];
			if ($fileArr["size"] > 0) {
				$file = fopen($fileName, "r");
				$tmpArr = array();
				while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
					if ( strtolower($column[0]) != 'zip' ) {
						$tmpArr[$column[0]] = $column;
					}
				}
				
				if ( !empty($tmpArr) ) {
					$this->createdata($tmpArr, $flag);
					return true;
				} else {
					return false;
				}
				
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	// function to match record in csv file with existing database and filter the non existing records to be inserted
	private function createdata($data = array(), $flag) {
		if ( !empty($data) ) {
			$tmp = array_keys($data);
			$tmp = implode(',',$tmp);
			if ( $flag ) {
				$sql = "select zip,product,recorded,org_user,modified_user from products where zip in (".$tmp.")";
			} else {
				$sql = "select zip,five_mile_population,recorded,org_user,modified_user from population where zip in (".$tmp.")";
			}
			$this->connect();
			$result = $this->mysqli->query($sql);
			if ($result -> num_rows > 0) {
				$tmpArr  = array();
				while($rows = mysqli_fetch_assoc($result)) {
					$tmpArr[$rows['zip']] = $rows;
				}
				
				if ( $result -> num_rows != count($data) ) {
					$zipArray = array();
					foreach ( $data as $key=>$val ) {
						if ( !isset($tmpArr[$key]) ) {
							$zipArray[] = $val; 
						}
					}
					
					return $this->createrecords($zipArray, $flag);
				} else {
					return true;
				}
			} else {
				return $this->createrecords($data, $flag);
			}
			
		} else {
			return false;
		}
	}
	
	// function to insert new records sent in csv file
	function createrecords( $data = array(), $flag ) {
		if ($flag) {
			$sql = "insert into products(zip,product,recorded,org_user,modified_user) values ";
		} else {
			$sql = "insert into population(zip,five_mile_population,recorded,org_user,modified_user) values ";
		}
		$data = array_values($data);
		foreach ($data as $key=>$val) {
			if ( $key > 0 ) {
				$sql .= " , ";
			}
			$sql .= "('".$val[0]."','".$val[1]."','".$val[2]."','".$val[3]."','".$val[4]."')";
		}
		$this->connect();
		if ( $result = $this->mysqli->query($sql) ) {
			return true;
		} else {
			return false;
		}
	}
	
}

?>
