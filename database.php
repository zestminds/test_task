<?php
class database {
	private $user = 'root';
	private $password = ''; //To be completed if you have set a password to root
	private $database = 'product_db'; //To be completed to connect to a database. The database must exist.
	private $port = "3308"; //Default must be NULL to use default port
	protected $mysqli;
	public function connect() {
		$this->mysqli = new mysqli('127.0.0.1', $this->user, $this->password, $this->database, $this->port);
		if ($this->mysqli->connect_error) {
			die('Connect Error (' . $this->mysqli->connect_errno . ') '
				. $this->mysqli->connect_error);
		}
		//echo '<p>Connection OK '. $this->mysqli->host_info.'</p>';
		//echo '<p>Server '.$this->mysqli->server_info.'</p>';
	}
	public function disconnect() {
		$this->mysqli->close();
	}
}

?>
